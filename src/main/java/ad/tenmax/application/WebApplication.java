package ad.tenmax.application;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import ad.tenmax.model.Ad;


@Configuration
@EnableAutoConfiguration
@ComponentScan( basePackages = {"ad.tenmax.application", "ad.tenmax.controller"} )
public class WebApplication {
	
   public static final SessionFactory sessionFactory = 
		   new org.hibernate.cfg.Configuration().configure("/resources/hibernate.cfg.xml")
			.buildSessionFactory();
	
	public static void main(String args[]) throws Exception{
	      //����SpringApplication
		SpringApplication.run(WebApplication.class, args);
			
	      ScheduledExecutorService exec = Executors.newSingleThreadScheduledExecutor();
	      exec.scheduleAtFixedRate(new Runnable() {
	        @Override
	        public void run() {
	        	JSONObject json;
	        	Session session = sessionFactory.openSession();
				try {
					json = readJsonFromUrl("https://beta-ssp.tenmax.io/supply/mobile/native/rmax-ad?rmaxSpaceId=c145f1cd389e49a5&dpid=bd4b9b7903cf40ce&v=1");
					String title = "";
					String description = "";
					String imageUrl = "";
					String iconUrl = "";
					String impressionLink = "";
					String clickUrl = "";
					
					JSONObject _native = json.getJSONObject("native");
					JSONArray assets = _native.getJSONArray("assets");
					JSONObject titleObj = (JSONObject) assets.get(0);
					title = titleObj.getJSONObject("title").getString("text");
					
					JSONObject iconObj = (JSONObject) assets.get(1);
					iconUrl = iconObj.getJSONObject("img").getString("url");
					
					JSONObject imageObj = (JSONObject) assets.get(2);
					imageUrl = imageObj.getJSONObject("img").getString("url");
					
					JSONObject descriptionObj = (JSONObject) assets.get(3);
					description = descriptionObj.getJSONObject("data").getString("value");
					
					JSONArray impressObj = _native.getJSONArray("impressionEvent");
					impressionLink = impressObj.toString();
					
					JSONObject clickObj = _native.getJSONObject("link");
					clickUrl = clickObj.getString("url");
					
					Ad ad = new Ad(title, description, imageUrl, iconUrl, impressionLink,clickUrl);
					session.beginTransaction();
					session.save(ad);
					session.getTransaction().commit();
					session = sessionFactory.openSession();
					session.beginTransaction();
						List result = session.createQuery("from Ad").list();
						for (Ad a : (List<Ad>) result) {
							System.out.println(a.getTitle());
						}
					session.getTransaction().commit();
//					System.out.println(json.toString());
					


				} catch (IOException | JSONException e) {
					session.close();
					e.printStackTrace();
				}
	            
	
	        }
	      }, 0, 60, TimeUnit.SECONDS);
	  }
	  private static String readAll(Reader rd) throws IOException {
	    StringBuilder sb = new StringBuilder();
	    int cp;
	    while ((cp = rd.read()) != -1) {
	      sb.append((char) cp);
	    }
	    return sb.toString();
	  }

	  public static JSONObject readJsonFromUrl(String url) throws IOException, JSONException {
	    InputStream is = new URL(url).openStream();
	    try {
	      BufferedReader rd = new BufferedReader(new InputStreamReader(is, Charset.forName("UTF-8")));
	      String jsonText = readAll(rd);
	      JSONObject json = new JSONObject(jsonText);
	      return json;
	    } finally {
	      is.close();
	    }
	  }
}