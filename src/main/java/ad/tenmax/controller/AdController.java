package ad.tenmax.controller;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.query.Query;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import ad.tenmax.application.WebApplication;
import ad.tenmax.model.Ad;


@Controller
public class AdController {

    @RequestMapping(value="/ad", method=RequestMethod.GET, produces = "text/html;charset=UTF-8")
    public @ResponseBody String queryAd(String keyword){
    	if(keyword == null) {
    		return "{}";
    	}
    	
    	String result = "{\"result\":[";
    	Session session = WebApplication.sessionFactory.openSession();
    	Query query = session.createQuery("from Ad where title LIKE :keyword", Ad.class);
    	query.setParameter("keyword", "%"+keyword+"%");

    	List<Ad> adList = query.getResultList();
		if (adList.size() == 0) {
			return "no result";
		}
		int i;
        
        for (i = 0; i < adList.size(); i++) {
            Ad ad = adList.get(i);
            if (i== adList.size()-1) {
            	result += ad.toString();
            } else {
            	result += ad.toString()+",";
            }
            
			System.out.println("List of Ad::"+ad.getTitle());
        }
         
		result = result + "]}";
        return result;
    }
}