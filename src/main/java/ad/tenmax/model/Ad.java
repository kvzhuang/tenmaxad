package ad.tenmax.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;

/**
 * Example account object that is persisted to disk by the DAO and other example classes.
 */
@Entity
@Table(name = "AD", uniqueConstraints={@UniqueConstraint(columnNames = {"title"})})
public class Ad {

	// for QueryBuilder to be able to find the fields
//	public static final String TITLE_FIELD_NAME = "title";
//	public static final String DESCRIPTION_FIELD_NAME = "description";
//	public static final String IMAGEURL_FIELD_NAME = "imageUrl";
//	public static final String ICONURL_FIELD_NAME = "iconUrl";
//	public static final String IMPRESSIONLINK_FIELD_NAME = "impressionLink";
//	public static final String CLICKURL_FIELD_NAME = "clickUrl";



	private int id;

	private String title;

	private String description;

	private String imageUrl;

	private String iconUrl;

	private String impressionLink;
	
	private String clickUrl;

	Ad() {

	}

	public Ad(String title, String description, String imageurl, String iconUrl ,String impressionlink, String clickurl) {
		this.title = title;
		this.description = description;
		this.imageUrl = imageurl;
		this.iconUrl = iconUrl;
		this.impressionLink = impressionlink;
		this.clickUrl = clickurl;
	}
	@Id
	@GeneratedValue
	@Column(name="ID")
	public int getId() {
		return id;
	}
	@Column(name="title")
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}
	@Column(name="description")
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	@Column(name="imageUrl", length=1000)
	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}
	@Column(name="iconUrl", length=1000)
	public String getIconUrl() {
		return iconUrl;
	}

	public void setIconUrl(String iconUrl) {
		this.iconUrl = iconUrl;
	}
	@Column(name="impressionLink", length=4000)
	public String getImpressionLink() {
		return impressionLink;
	}

	public void setImpressionLink(String impressionLink) {
		this.impressionLink = impressionLink;
	}
	@Column(name="clickUrl", length=4000)
	public String getClickUrl() {
		return clickUrl;
	}

	public void setClickUrl(String clickUrl) {
		this.clickUrl = clickUrl;
	}

	public void setId(int id) {
		this.id = id;
	}
	

	
	@Override
	public int hashCode() {
		return title.hashCode();
	}
	@Override
	public String toString() {
		ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
		String json;
		try {
			json = ow.writeValueAsString(this);
			return json;
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return this.title;
	}

	@Override
	public boolean equals(Object other) {
		if (other == null || other.getClass() != getClass()) {
			return false;
		}
		return title.equals(((Ad) other).title);
	}
}