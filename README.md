Repo: https://bitbucket.org/kvzhuang/tenmaxad
Eclipse Version: Neon Release (4.6.0)
java version "1.8.0_101"

Gradle Setting
compile("org.springframework.boot:spring-boot-starter-web:1.1.5.RELEASE")
compile("org.hibernate:hibernate-core:5.2.1.Final")
compile group: 'com.h2database', name: 'h2', version: '1.4.192'
compile group: 'org.json', name: 'json', version: '20090211'

